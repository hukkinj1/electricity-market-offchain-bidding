pragma solidity ^0.4.11;

import "./SmartMeters.sol";


contract ElectricityMarket {

    enum ContractState { NotCreated, Created, Accepted, WaitingForBuyerReport,
        WaitingForSellerReport, ReadyForWithdrawal, Resolved, TimedOut }

    struct Contract {
        address seller;
        address sellerSmartMeter;
        address buyer;
        address buyerSmartMeter;
        uint price;
        uint electricityAmount;
        uint startTime;
        uint endTime;
        bool sellerReport;  // true if everything went OK
        bool buyerReport;  // true if everything went OK
        ContractState state;
    }

    modifier contractInState(uint id, ContractState state) {
        require(contracts[id].state == state);
        _;
    }

    modifier waitingForSellerReport(uint id) {
        if ((contracts[id].state != ContractState.Accepted) && (contracts[id].state != ContractState.WaitingForSellerReport)) {
            throw;
        }
        _;
    }

    modifier waitingForBuyerReport(uint id) {
        if ((contracts[id].state != ContractState.Accepted) && (contracts[id].state != ContractState.WaitingForBuyerReport)) {
            throw;
        }
        _;
    }

    modifier ownsSmartMeter(address owner, address smartMeter) {
        require(owner == smartMeters.owner(smartMeter));
        _;
    }

    modifier buyerSmartMeterOnly(uint id) {
        require(msg.sender == contracts[id].buyerSmartMeter);
        _;
    }

    modifier sellerSmartMeterOnly(uint id) {
        require(msg.sender == contracts[id].sellerSmartMeter);
        _;
    }

    modifier condition(bool c) {
        require(c);
        _;
    }

    modifier costs(uint price) {
        require(msg.value == price);
        _;
    }

    mapping (uint => Contract) contracts;
    mapping (address => uint[]) contractsBySmartMeter;
    SmartMeters smartMeters;


    // Minimum time from block.timestamp to startTime. The time needs to be long
    // enough, so that a few blocks are created in between, so that the smart
    // meters can be sure that the transmission is approved by the blockchain
    uint minTimeFromAcceptedToStart = 60;  // 1 minute
    // Maximum time from endTime to smart meters reporting about how the
    // transmission went.
    uint maxTimeFromEndToReportDeadline = 1800;  // 30 minutes

    event LogOffer(address seller, uint id, uint price, uint electricityAmount, uint startTime, uint endTime, address sellerSmartMeter);
    event LogAcceptOffer(address seller, uint id, uint price, uint electricityAmount, uint startTime, uint endTime, address sellerSmartMeter, address buyer, address buyerSmartMeter);
    event LogResolved(uint id, address seller, address buyer, address recipient);

    function ElectricityMarket() {
        smartMeters = new SmartMeters();
    }

    function acceptOffer(bytes32 r, bytes32 s, uint8 v, uint256 id, uint256 price, uint256 electricityAmount, uint256 startTime, uint256 endTime, address sellerSmartMeter, address buyerSmartMeter) payable
        condition(startTime < endTime)
        costs(price)
        ownsSmartMeter(msg.sender, buyerSmartMeter)
    {
        contractNotCreatedCheck(id);
        noOverlappingContractsCheck(sellerSmartMeter, startTime, endTime);
        contractTimeoutCheck(startTime);

        address seller = getOfferSigner(r, s, v, id, price, electricityAmount, startTime, endTime, sellerSmartMeter);
        require(seller == smartMeters.owner(sellerSmartMeter));

        storeNewOffer(id, price, electricityAmount, startTime, endTime, sellerSmartMeter, seller, buyerSmartMeter, msg.sender);
        LogOffer(seller, id, price, electricityAmount, startTime, endTime, sellerSmartMeter);
        LogAcceptOffer(seller, id, price, electricityAmount, startTime, endTime, sellerSmartMeter, msg.sender, buyerSmartMeter);
    }

    function getOfferSigner(bytes32 r, bytes32 s, uint8 v, uint256 id, uint256 price, uint256 electricityAmount, uint256 startTime, uint256 endTime, address sellerSmartMeter) private constant returns (address) {
        // The prefix used by web3.eth.sign method. The number in the end is the
        // amount of bytes in the message.
        bytes memory prefix = "\x19Ethereum Signed Message:\n180";

        bytes32 hash = keccak256(prefix, id, price, electricityAmount, startTime, endTime, sellerSmartMeter);
        return ecrecover(hash, v, r, s);
    }

    function sellerReport(uint id, bool report)
        sellerSmartMeterOnly(id)
        waitingForSellerReport(id)
    {
        if (hasReportDeadlineExpired(id)) {
            contracts[id].state = ContractState.ReadyForWithdrawal;
            return;
        }
        contracts[id].sellerReport = report;
        contracts[id].state = (contracts[id].state == ContractState.Accepted) ? ContractState.WaitingForBuyerReport : ContractState.ReadyForWithdrawal;
    }

    function buyerReport(uint id, bool report)
        buyerSmartMeterOnly(id)
        waitingForBuyerReport(id)
    {
        if (hasReportDeadlineExpired(id)) {
            contracts[id].state = ContractState.ReadyForWithdrawal;
            return;
        }
        contracts[id].buyerReport = report;
        contracts[id].state = (contracts[id].state == ContractState.Accepted) ? ContractState.WaitingForSellerReport : ContractState.ReadyForWithdrawal;
    }

    function withdraw(uint id)
    {
        if (contracts[id].state != ContractState.ReadyForWithdrawal) {
            makeReadyForWithdrawal(id);
        }
        contracts[id].state = ContractState.Resolved;

        address recipient;

        if (!contracts[id].sellerReport) {
            recipient = contracts[id].buyer;
        }
        else if (contracts[id].buyerReport) {
            recipient = contracts[id].seller;
        }
        else {
            recipient = this;
        }

        LogResolved(id, contracts[id].seller, contracts[id].buyer, recipient);

        if (recipient != address(this)) {
            if (!recipient.send(contracts[id].price)) {
                throw;
            }
        }
    }

    // Assume that startTime < endTime for both timestamp pairs
    function doTimeslotsOverlap(uint startTime1, uint endTime1, uint startTime2, uint endTime2) private constant returns (bool) {
        if ((endTime1 < startTime2) || (endTime2 < startTime1)) {
            return false;
        }
        return true;
    }

    function hasReportDeadlineExpired(uint id) private constant returns (bool) {
        if ((contracts[id].endTime + maxTimeFromEndToReportDeadline) > now) {
            return false;
        }
        return true;
    }

    function storeNewOffer(uint id, uint price, uint electricityAmount, uint startTime, uint endTime, address sellerSmartMeter, address seller, address buyerSmartMeter, address buyer) private {
        contracts[id].seller = seller;
        contracts[id].price = price;
        contracts[id].electricityAmount = electricityAmount;
        contracts[id].startTime = startTime;
        contracts[id].endTime = endTime;
        contracts[id].sellerSmartMeter = sellerSmartMeter;
        contracts[id].state = ContractState.Accepted;
        contracts[id].buyer = buyer;
        contracts[id].buyerSmartMeter = buyerSmartMeter;

        contractsBySmartMeter[sellerSmartMeter].push(id);
    }

    function noOverlappingContractsCheck(address smartMeter, uint startTime, uint endTime) private constant {
        for (uint i = 0; i < contractsBySmartMeter[smartMeter].length; i++) {
            Contract c = contracts[contractsBySmartMeter[smartMeter][i]];
            if (doTimeslotsOverlap(startTime, endTime, c.startTime, c.endTime)) {
                throw;
            }
        }
    }

    function contractTimeoutCheck(uint startTime) private constant {
        require((now + minTimeFromAcceptedToStart) <= startTime);
    }

    function contractNotCreatedCheck(uint id) private constant {
        require(contracts[id].state == ContractState.NotCreated);
    }

    // Change the state ReadyForWithdrawal if report deadline has expired. If
    // not succesful for any reason, then throw.
    function makeReadyForWithdrawal(uint id) private {
        if ((contracts[id].state == ContractState.Accepted
            || contracts[id].state == ContractState.WaitingForSellerReport
            || contracts[id].state == ContractState.WaitingForBuyerReport)
            && hasReportDeadlineExpired(id))
        {
            contracts[id].state = ContractState.ReadyForWithdrawal;
            return;
        }
        throw;
    }

    function getSeller(uint id) constant returns (address) {
        return contracts[id].seller;
    }

    function getBuyer(uint id) constant returns (address) {
        return contracts[id].buyer;
    }

    function getBuyerReport(uint id) constant returns (bool) {
        return contracts[id].buyerReport;
    }

    function getSellerReport(uint id) constant returns (bool) {
        return contracts[id].sellerReport;
    }

    function isCreated(uint id) constant returns (bool) {
        return contracts[id].state != ContractState.NotCreated;
    }

    function getState(uint id) constant returns (ContractState) {
        return contracts[id].state;
    }

    function getSmartMetersContract() constant returns (address) {
        return smartMeters;
    }

}
