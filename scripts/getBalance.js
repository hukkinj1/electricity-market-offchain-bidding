const Constants = require("./constants.js");

let account;
let input = process.argv[2];
if (input === "") {
	console.log("Specify address");
	return;
}
else if (input === "contract") {
	account = Constants.marketAddress;
}

const balance = Constants.web3.eth.getBalance(account).toNumber();
console.log("Account: " + account);
console.log("Balance: " + balance);