const Constants = require("./constants.js");
const web3 = Constants.web3;

function pad(number, length) {
    let str = '' + number;
    while (str.length < length) str = '0' + str;
    return str;
}

// All parameters are integers, except ETH addresses, which are hex strings
function getHexData(id, price, electricityAmount, startTime, endTime, sellerSmartMeter) {
	return "0x" + pad(id.toString(16), 64) + pad(price.toString(16), 64) + pad(electricityAmount.toString(16), 64) + pad(startTime.toString(16), 64) + pad(endTime.toString(16), 64) + sellerSmartMeter.substring(2);
}

function getR(signature) {
	const signatureWithoutHexPrefix = signature.substring(2);
	return "0x" + signatureWithoutHexPrefix.substring(0, 64);
}

function getS(signature) {
	const signatureWithoutHexPrefix = signature.substring(2);
	return "0x" + signatureWithoutHexPrefix.substring(64, 128);
}

function getV(signature) {
	const signatureWithoutHexPrefix = signature.substring(2);
	// Add 27 to the value of v to make it work with ecrecover()
	return "0x" + (parseInt(signatureWithoutHexPrefix.substring(128, 130), 16) + 27).toString(16);
}

module.exports = [{
	"id": 1349,
	"startTime": 1800011110,
	"endTime": 1800011111,
	"price": 1200000000,
	"electricityAmount": 110,
	"sellerSmartMeter": Constants.accounts.sellerSmartMeter,
	"buyerSmartMeter": Constants.accounts.buyerSmartMeter,
	"signature": function() {
		return web3.eth.sign(Constants.accounts.seller, getHexData(this.id, this.price, this.electricityAmount, this.startTime, this.endTime, this.sellerSmartMeter));
	},
	"r": function() {
		return getR(this.signature());
	},
	"s": function() {
		return getS(this.signature());
	},
	"v": function() {
		return getV(this.signature());
	}
},{
	"id": 1350,
	"startTime": 1800011115,
	"endTime": 1800011120,
	"price": 1200000000,
	"electricityAmount": 110,
	"sellerSmartMeter": Constants.accounts.sellerSmartMeter,
	"buyerSmartMeter": Constants.accounts.buyerSmartMeter,
	"signature": function() {
		return web3.eth.sign(Constants.accounts.seller, getHexData(this.id, this.price, this.electricityAmount, this.startTime, this.endTime, this.sellerSmartMeter));
	},
	"r": function() {
		return getR(this.signature());
	},
	"s": function() {
		return getS(this.signature());
	},
	"v": function() {
		return getV(this.signature());
	}
},{
	"id": 1351,
	"startTime": 1800011125,
	"endTime": 1800011130,
	"price": 1200000000,
	"electricityAmount": 110,
	"sellerSmartMeter": Constants.accounts.sellerSmartMeter,
	"buyerSmartMeter": Constants.accounts.buyerSmartMeter,
	"signature": function() {
		return web3.eth.sign(Constants.accounts.seller, getHexData(this.id, this.price, this.electricityAmount, this.startTime, this.endTime, this.sellerSmartMeter));
	},
	"r": function() {
		return getR(this.signature());
	},
	"s": function() {
		return getS(this.signature());
	},
	"v": function() {
		return getV(this.signature());
	}

},{
	"id": 1352,
	"startTime": 1800011200,
	"endTime": 1800011300,
	"price": 1200000000,
	"electricityAmount": 110,
	"sellerSmartMeter": Constants.accounts.sellerSmartMeter,
	"buyerSmartMeter": Constants.accounts.buyerSmartMeter,
	"signature": function() {
		return web3.eth.sign(Constants.accounts.seller, getHexData(this.id, this.price, this.electricityAmount, this.startTime, this.endTime, this.sellerSmartMeter));
	},
	"r": function() {
		return getR(this.signature());
	},
	"s": function() {
		return getS(this.signature());
	},
	"v": function() {
		return getV(this.signature());
	}
},{
	"id": 1353,
	"startTime": 1800011301,
	"endTime": 1800011324,
	"price": 1200000000,
	"electricityAmount": 110,
	"sellerSmartMeter": Constants.accounts.sellerSmartMeter,
	"buyerSmartMeter": Constants.accounts.buyerSmartMeter,
	"signature": function() {
		return web3.eth.sign(Constants.accounts.seller, getHexData(this.id, this.price, this.electricityAmount, this.startTime, this.endTime, this.sellerSmartMeter));
	},
	"r": function() {
		return getR(this.signature());
	},
	"s": function() {
		return getS(this.signature());
	},
	"v": function() {
		return getV(this.signature());
	}
},{
	"id": 1354,
	"startTime": 1800011350,
	"endTime": 1800011352,
	"price": 1200000000,
	"electricityAmount": 110,
	"sellerSmartMeter": Constants.accounts.sellerSmartMeter,
	"buyerSmartMeter": Constants.accounts.buyerSmartMeter,
	"signature": function() {
		return web3.eth.sign(Constants.accounts.seller, getHexData(this.id, this.price, this.electricityAmount, this.startTime, this.endTime, this.sellerSmartMeter));
	},
	"r": function() {
		return getR(this.signature());
	},
	"s": function() {
		return getS(this.signature());
	},
	"v": function() {
		return getV(this.signature());
	}
},{
	"id": 1355,
	"startTime": 1800011401,
	"endTime": 1800011420,
	"price": 1200000000,
	"electricityAmount": 110,
	"sellerSmartMeter": Constants.accounts.sellerSmartMeter,
	"buyerSmartMeter": Constants.accounts.buyerSmartMeter,
	"signature": function() {
		return web3.eth.sign(Constants.accounts.seller, getHexData(this.id, this.price, this.electricityAmount, this.startTime, this.endTime, this.sellerSmartMeter));
	},
	"r": function() {
		return getR(this.signature());
	},
	"s": function() {
		return getS(this.signature());
	},
	"v": function() {
		return getV(this.signature());
	}
},{
	"id": 1356,
	"startTime": 1800011422,
	"endTime": 1800011430,
	"price": 1200000000,
	"electricityAmount": 110,
	"sellerSmartMeter": Constants.accounts.sellerSmartMeter,
	"buyerSmartMeter": Constants.accounts.buyerSmartMeter,
	"signature": function() {
		return web3.eth.sign(Constants.accounts.seller, getHexData(this.id, this.price, this.electricityAmount, this.startTime, this.endTime, this.sellerSmartMeter));
	},
	"r": function() {
		return getR(this.signature());
	},
	"s": function() {
		return getS(this.signature());
	},
	"v": function() {
		return getV(this.signature());
	}
},{
	"id": 1357,
	"startTime": 1800011499,
	"endTime": 1800011501,
	"price": 1200000000,
	"electricityAmount": 110,
	"sellerSmartMeter": Constants.accounts.sellerSmartMeter,
	"buyerSmartMeter": Constants.accounts.buyerSmartMeter,
	"signature": function() {
		return web3.eth.sign(Constants.accounts.seller, getHexData(this.id, this.price, this.electricityAmount, this.startTime, this.endTime, this.sellerSmartMeter));
	},
	"r": function() {
		return getR(this.signature());
	},
	"s": function() {
		return getS(this.signature());
	},
	"v": function() {
		return getV(this.signature());
	}
},{
	"id": 1358,
	"startTime": 1800011502,
	"endTime": 1800011520,
	"price": 1200000000,
	"electricityAmount": 110,
	"sellerSmartMeter": Constants.accounts.sellerSmartMeter,
	"buyerSmartMeter": Constants.accounts.buyerSmartMeter,
	"signature": function() {
		return web3.eth.sign(Constants.accounts.seller, getHexData(this.id, this.price, this.electricityAmount, this.startTime, this.endTime, this.sellerSmartMeter));
	},
	"r": function() {
		return getR(this.signature());
	},
	"s": function() {
		return getS(this.signature());
	},
	"v": function() {
		return getV(this.signature());
	}
}];