/* A random integer between the specified values. The value is no lower than min
 * (or the next integer greater than min if min isn't an integer), and is less
 * than (but not equal to) max.
 */
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

function blockingBusyWait(seconds) {
	const waitTill = new Date(new Date().getTime() + seconds * 1000);
	while(waitTill > new Date()){}
}

function pad(number, length) {
    let str = '' + number;
    while (str.length < length) str = '0' + str;
    return str;
}

// All parameters are integers, except ETH addresses, which are hex strings
function getHexData(id, price, electricityAmount, startTime, endTime, sellerSmartMeter) {
	return "0x" + pad(id.toString(16), 64) + pad(price.toString(16), 64) + pad(electricityAmount.toString(16), 64) + pad(startTime.toString(16), 64) + pad(endTime.toString(16), 64) + sellerSmartMeter.substring(2);
}

function getR(signature) {
	const signatureWithoutHexPrefix = signature.substring(2);
	return "0x" + signatureWithoutHexPrefix.substring(0, 64);
}

function getS(signature) {
	const signatureWithoutHexPrefix = signature.substring(2);
	return "0x" + signatureWithoutHexPrefix.substring(64, 128);
}

function getV(signature) {
	const signatureWithoutHexPrefix = signature.substring(2);
	// Add 27 to the value of v to make it work with ecrecover()
	return "0x" + (parseInt(signatureWithoutHexPrefix.substring(128, 130), 16) + 27).toString(16);
}

const Constants = require("../constants.js");
const market = Constants.market;
const offers = require("../offer-objects.js");

let numberOfOffers = parseInt(process.argv[2]);
if (isNaN(numberOfOffers)) {
	numberOfOffers = 1;
}
let totalGas = 0;
const firstId = 1260;


const issuer = Constants.accounts.issuer;
const sellerSmartMeter = Constants.accounts.sellerSmartMeter;
const buyerSmartMeter = Constants.accounts.buyerSmartMeter;
const price = 100;
const electricityAmount = 12505;
const buyer = Constants.accounts.buyer;
const gas = 3000000;

for (let i = firstId; i < numberOfOffers + firstId; i++) {
	const id = i;
	const startTime = 1800000000 + i*2;
	const endTime = startTime + 1;
	const signature = Constants.web3.eth.sign(Constants.accounts.seller, getHexData(id, price, electricityAmount, startTime, endTime, sellerSmartMeter));
	const r = getR(signature);
	const s = getS(signature);
	const v = getV(signature);

    totalGas += market.acceptOffer.estimateGas(r, s, v, id, price, electricityAmount, startTime, endTime, sellerSmartMeter, buyerSmartMeter,
		{from: buyer, gas: gas, value: price});
    blockingBusyWait(4);
    market.acceptOffer.sendTransaction(r, s, v, id, price, electricityAmount, startTime, endTime, sellerSmartMeter, buyerSmartMeter,
		{from: buyer, gas: gas, value: price});
    blockingBusyWait(4);
}


for (let i = firstId; i < numberOfOffers + firstId; i++) {
	const id = i;

    totalGas += market.sellerReport.estimateGas(id, true,
		{from: sellerSmartMeter, gas: gas});
    blockingBusyWait(4);

    market.sellerReport.sendTransaction(id, true,
		{from: sellerSmartMeter, gas: gas});
    blockingBusyWait(4);
}

for (let i = firstId; i < numberOfOffers + firstId; i++) {
	const id = i;

    totalGas += market.buyerReport.estimateGas(id, true,
		{from: buyerSmartMeter, gas: gas});
    blockingBusyWait(4);

    market.buyerReport.sendTransaction(id, true,
		{from: buyerSmartMeter, gas: gas});
    blockingBusyWait(4);
}

for (let i = firstId; i < numberOfOffers + firstId; i++) {
	const id = i;

    totalGas += market.withdraw.estimateGas(id,
		{from: issuer, gas: gas});
    blockingBusyWait(4);

    market.withdraw.sendTransaction(id,
		{from: issuer, gas: gas});
    blockingBusyWait(4);
}

console.log(totalGas);