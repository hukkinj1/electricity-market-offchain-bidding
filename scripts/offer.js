const Constants = require("./constants.js");
const market = Constants.market;
const web3 = Constants.web3;

let offers = require("./offer-objects.js");

let offerIndex = parseInt(process.argv[2]);
if (isNaN(offerIndex)) {
	offerIndex = 0;
}

const offer = offers[offerIndex];

console.log("id:\t\t\t" + offer.id)
console.log("price:\t\t\t" + offer.price)
console.log("electricityAmount:\t" + offer.electricityAmount)
console.log("startTime:\t\t" + offer.startTime)
console.log("endTime:\t\t" + offer.endTime)
console.log("sellerSmartMeter:\t" + offer.sellerSmartMeter)
console.log("r:\t\t\t" + offer.r())
console.log("s:\t\t\t" + offer.s())
console.log("v:\t\t\t" + offer.v())